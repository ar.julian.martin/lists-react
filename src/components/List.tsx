import { Item } from "./Item";
import { TItem } from "../types";

interface ListProps {
  items: TItem[];
  onMoveItem: (fromIndex: number, targetIndex: number) => void;
  onMoveItemLeft: (elementIndex: number) => void;
  onMoveItemRight: (elementIndex: number) => void;
}

export const List = ({
  items,
  onMoveItem,
  onMoveItemLeft,
  onMoveItemRight
}: ListProps) => {
  const handleMoveUp = (index: number) => onMoveItem(index, index - 1);
  const handleMoveDown = (index: number) => onMoveItem(index, index + 1);

  return (
    <div className="list">
      {items.map((item, i) => {
        if (!item) return null;
        return (
          <Item
            key={item.id}
            info={item}
            onMoveUp={() => handleMoveUp(i)}
            onMoveDown={() => handleMoveDown(i)}
            onMoveLeft={() => onMoveItemLeft(i)}
            onMoveRight={() => onMoveItemRight(i)}
          />
        );
      })}
    </div>
  );
};
