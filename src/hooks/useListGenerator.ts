import { useState } from "react";

import "minifaker/locales/en";

import { moveItemInList, moveItemsBetweenLists } from "../utils";
import { TItem } from "../types";

export const useListGenerator = (initialItems: TItem[][]) => {
  // Estado general. Lista de listas.
  const [lists, setLists] = useState(initialItems);

  // Factory de una funcion para mover internamente los elementos de una lista.
  const handleMoveItemBuilder = (listIndex: number) => {
    const list = lists[listIndex];
    return (fromIndex: number, targetIndex: number) => {
      const updatedList = moveItemInList({ list, fromIndex, targetIndex });
      setLists((lists) => [
        ...lists.slice(0, listIndex),
        updatedList,
        ...lists.slice(listIndex + 1)
      ]);
    };
  };

  // Funcion para mover un elemento hacia la lista de la izquierda.
  const handleMoveItemLeft = (listIndex: number, elementIndex: number) => {
    // Si es la primer lista no puedo mover el elemento.
    if (listIndex === 0) return;

    const from = lists[listIndex];
    const to = lists[listIndex - 1];

    const [newFrom, newTo] = moveItemsBetweenLists({
      from,
      to,
      index: elementIndex
    });

    setLists((lists) => [
      ...lists.slice(0, listIndex - 1),
      newTo,
      newFrom,
      ...lists.slice(listIndex + 1)
    ]);
  };

  // Funcion para mover un elemento hacia la lista de la derecha.
  const handleMoveItemRight = (listIndex: number, elementIndex: number) => {
    // Si es la ultima lista no puedo mover el elemento.
    if (listIndex === lists.length - 1) return;

    const from = lists[listIndex];
    const to = lists[listIndex + 1];

    const [newFrom, newTo] = moveItemsBetweenLists({
      from,
      to,
      index: elementIndex
    });

    setLists((lists) => [
      ...lists.slice(0, listIndex),
      newFrom,
      newTo,
      ...lists.slice(listIndex + 2)
    ]);
  };

  return {
    lists,
    handleMoveItemBuilder,
    handleMoveItemLeft,
    handleMoveItemRight
  };
};
