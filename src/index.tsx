import { render } from "react-dom";
import { Main } from "./Main";

import "./styles/index.css";

function App() {
  return <Main />;
}

const rootElement = document.getElementById("root");
render(<App />, rootElement);
